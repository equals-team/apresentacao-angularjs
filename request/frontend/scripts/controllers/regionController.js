(function() {

	var app = angular.module('appModule');

	app.controller('RegionController', function($scope, config, regionService) {

		var tree;
		$scope.tree_data = [];
		
		$scope.list = function() {

			regionService.list(function(data){

				$scope.tree_data = getTree(data, 'DemographicId', 'ParentId');

			}, 
			function(error){
				console.log(error);
			});

		};

        $scope.tree_control = tree = {};
        $scope.expanding_property = {
            field: "Name",
            displayName: "Demographic Name",
            sortable: true,
            filterable: true,
            cellTemplate: "<i>{{row.branch[expandingProperty.field]}}</i>"
        };
        $scope.col_defs = [
            {
                field: "Description",
                sortable: true,
                sortingType: "string"
            },
            {
                field: "Area",
                sortable: true,
                sortingType: "number",
                filterable: true
            },
            {
                field: "Population",
                sortable: true,
                sortingType: "number"
            },
            {
                field: "TimeZone",
                displayName: "Time Zone",
                cellTemplate: "<strong>{{row.branch[col.field]}}</strong>"
            }
        ];
        $scope.tree_handler = function (element) {
            console.log('Você clicou no elemento: ', element);
        }

        function getTree(data, primaryIdName, parentIdName) {
            if (!data || data.length == 0 || !primaryIdName || !parentIdName)
                return [];

            var tree = [],
                rootIds = [],
                item = data[0],
                primaryKey = item[primaryIdName],
                treeObjs = {},
                parentId,
                parent,
                len = data.length,
                i = 0;

            while (i < len) {
                item = data[i++];
                primaryKey = item[primaryIdName];
                treeObjs[primaryKey] = item;
                parentId = item[parentIdName];

                if (parentId) {
                    parent = treeObjs[parentId];

                    if (parent.children) {
                        parent.children.push(item);
                    } else {
                        parent.children = [item];
                    }
                } else {
                    rootIds.push(primaryKey);
                }
            }

            for (var i = 0; i < rootIds.length; i++) {
                tree.push(treeObjs[rootIds[i]]);
            }
            ;

            return tree;
        }

	});

}).call(this);