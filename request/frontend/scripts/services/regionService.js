(function() {

	var app = angular.module('appModule');

	app.service('regionService', function(requestUtil, config) {

		var service = {};

		service.list = function(successCallback, errorCallback) {

			requestUtil.get(config.BASE_URL + "region", successCallback, errorCallback);

		};

		return service;
	});

})();