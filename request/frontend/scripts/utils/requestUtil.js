(function(){

	var app = angular.module('appModule');

	app.service('requestUtil', function($http, $rootScope) {
		
		var service = {};

		service.get = function(url, success, error) {

			return $http.get(url)
				.success(function(data){

					if(success) {
						success.call(null, data);
					}

				})
				.error(function(data){

					if(error) {
						error.call(null, data);
					}

				});

		};

		return service;

	});

})();